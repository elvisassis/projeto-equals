
# Projeto Equals
## Tecnologias utilizadas
 - Java 11
 - Spring Boot 2.1.5
 - VueJs
 - node 8.11.2
 - Postgresql 11
## Configuração ambiente
 - Após clonar o projeto vá até as pasta frontend e rode o comando **npm install** para instalar as dependências do frontend.
 - Depois vá até a pasta backend e rode o comando **./gradlew build** para instalar as depedências do backend.
 - Após esse passo ainda na pasta backend vá em **/backend/src/main/resources/db** e rode o script *create_db.sql* para a criação do banco e do usuário da aplicação.
 - O arquivo que será processado deverar estar na pasta */backend/arquivo/em_processamento*, após o mesmo ser processado ele será excluido, o processamento e feito por um job que roda de 30 em 30 segundos.
 - Para rodar o projeto execute na pasta backend o comando **./gradlew bootRun** o scritp que cria as tabelas irá rodar automaticamente após esse comando.
 - Na pasta frontend rode o comando **npm run dev ** após esse comando a aplicação poderá ser acessada na url : *http://localhost:8081*.

