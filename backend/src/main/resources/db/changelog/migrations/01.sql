--liquibase formatted sql

--changeset dev:01 splitStatements:false



create table header
(
	id SERIAL not null,
	tipo_registro numeric,
	estabelecimento numeric,
	data_processamento TIMESTAMP WITHOUT TIME ZONE,
	periodo_inicial TIMESTAMP WITHOUT TIME ZONE,
	periodo_final TIMESTAMP WITHOUT TIME ZONE,
	sequencia numeric,
	empresa_adquirente CHARACTER VARYING(100),
	tipo_extrato numeric,
	filler CHARACTER VARYING(100),
	versao_layout CHARACTER VARYING(100),
	versao_release CHARACTER VARYING(100),
	 CONSTRAINT pk_header PRIMARY KEY (id)

);

create table trailler
(
	id serial not null,
	tipo_registro numeric,
	total_registros numeric,
	CONSTRAINT pk_trailler PRIMARY KEY (id)

);

create table transacao
(
	id SERIAL not null,
	id_header integer,
	id_trailler integer,
	tipo_registro numeric,
	estabelecimento numeric,
	data_inicio_transacao TIMESTAMP WITHOUT TIME ZONE,
	data_evento TIMESTAMP WITHOUT TIME ZONE,
	hora_evento TIME WITHOUT TIME ZONE,
	tipo_evento numeric,
	tipo_transacao numeric,
	numero_serie_leitor CHARACTER VARYING(100),
	codigo_transacao CHARACTER VARYING(100),
	codigo_pedido CHARACTER VARYING(100),
	valor_total_transacao numeric,
	valor_parcela_liquida_ou_total numeric,
	pagamento CHARACTER VARYING(100),
	plano CHARACTER VARYING(100),
	parcela CHARACTER VARYING(100),
	qtd_parcelas_transacao numeric,
	data_prevista_pagamento TIMESTAMP WITHOUT TIME ZONE,
	taxa_parcelamento_comprador numeric,
	tarifa_boleto_comprador numeric,
	valor_original_transacao numeric,
	taxa_parcelamento_vendedor numeric,
	taxa_intermediacao numeric,
	tarifa_intermediacao numeric,
	tarifa_boleto_vendedor numeric,
	repasse_aplicacao numeric,
	valor_liquido_transacao numeric,
	status_pagamento numeric,
	filler CHARACTER VARYING(100),
	meio_pagamento numeric,
	instituicao_financeira CHARACTER VARYING(100),
	canal_entrada CHARACTER VARYING(100),
	leitor numeric,
	meio_captura numeric,
	numero_logico CHARACTER VARYING(100),
	nsu CHARACTER VARYING(100),
	cartao_bin CHARACTER VARYING(100),
	cartao_holder CHARACTER VARYING(100),
	codigo_autorizacao CHARACTER VARYING(100),
	codigo_cv CHARACTER VARYING(100),
	CONSTRAINT pk_vendas PRIMARY KEY (id),
	CONSTRAINT fk_header FOREIGN KEY (id_header)
		REFERENCES header (id),
	CONSTRAINT fk_trailler FOREIGN KEY (id_trailler)
		REFERENCES trailler (id)

)
