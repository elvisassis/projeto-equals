
-- Executar este script com o usuário "postgres".

-- Criar usuário para o banco de dados.

CREATE ROLE projeto_equals LOGIN
  PASSWORD 'projeto_equals'
  SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;


-- Criar banco de dados de desenvolvimento.

CREATE DATABASE projeto_equals
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

-- Criar banco de dados de teste.

CREATE DATABASE projeto_equals_test
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;
