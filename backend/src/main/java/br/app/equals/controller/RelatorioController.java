package br.app.equals.controller;

import br.app.equals.model.dto.TransacaoDTO;
import br.app.equals.model.entities.Transacao;
import br.app.equals.service.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RelatorioController {

    @Autowired
    private TransacaoService transacaoService;


    @PostMapping("/relatorio-vendas")
    public ResponseEntity<List<TransacaoDTO>> relatorioVendas(@RequestBody TransacaoDTO transacaoDTO) {

        return ResponseEntity.ok().body(transacaoService.findByFiltros(transacaoDTO));

    }

}
