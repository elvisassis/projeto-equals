package br.app.equals.configuracao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConfiguracaStone {

	private Header header;
	private Transacao transacao;
	private Trailler trailler;

	public class Header {

		public TipoRegistro tipoRegistro;
		public Estabelecimento estabelecimento;
		public DataProcessamento dataProcessamento;
		public PeriodoInicial periodoInicial;
		public PeriodoFinal periodoFinal;
		public Sequencia sequencia;
		public EmpresaAdiquirente empresaAdiquirente;
		public TipoExtrato tipoExtrato;
		public Filler filler;
		public VersaoLayout versaoLayout;
		public VersaoRelease versaoRelease;

		public class TipoRegistro{
			public Integer inicio;
			public Integer fim;
		}

		public class Estabelecimento{
			public Integer inicio;
			public Integer fim;
		}

		public class DataProcessamento{
			public Integer inicio;
			public Integer fim;
		}

		public class PeriodoInicial{
			public Integer inicio;
			public Integer fim;
		}

		public class PeriodoFinal{
			public Integer inicio;
			public Integer fim;
		}

		public class Sequencia{
			public Integer inicio;
			public Integer fim;
		}

		public class EmpresaAdiquirente{
			public Integer inicio;
			public Integer fim;
		}

		public class TipoExtrato{
			public Integer inicio;
			public Integer fim;
		}
		public class Filler{
			public Integer inicio;
			public Integer fim;
		}
		public class VersaoLayout{
			public Integer inicio;
			public Integer fim;
		}
		public class VersaoRelease{
			public Integer inicio;
			public Integer fim;
		}

	}

	public class Transacao {

		public TipoRegistro tipoRegistro;
		public Estabelecimento estabelecimento;
		public DataInicialTransacao dataInicialTransacao;
		public DataEvento dataEvento;
		public HoraEvento horaEvento;
		public TipoEvento tipoEvento;
		public TipoTransacao tipoTransacao;
		public NumeroSerieLeitor numeroSerieLeitor;
		public CodigoTransacao codigoTransacao;
		public CodigoPedido codigoPedido;
		public ValorTotalTransacao valorTotalTransacao;
		public ValorParcelaOuLiquidoTotal valorParcelaOuLiquidoTotal;
		public Pagamento pagamento;
		public Plano plano;
		public Parcela parcela;
		public QtdParcelas qtdParcelas;
		public DataPrevistoPagamento dataPrevistoPagamento;
		public TaxaParcelamentoComprador taxaParcelamentoComprador;
		public TarifaBoletoComprador tarifaBoletoComprador;
		public ValorOriginalTransacao valorOriginalTransacao;
		public TaxaParcelamentoVendedor taxaParcelamentoVendedor;
		public TaxaIntermediacao taxaIntermediacao;
		public TarifaItermediacao tarifaItermediacao;
		public TarifaBoletoVendedor tarifaBoletoVendedor;
		public RepasseAplicacao repasseAplicacao;
		public ValorLiquidoTransacao valorLiquidoTransacao;
		public StatusPagamento statusPagamento;
		public Filler filler;
		public MeioPagamento meioPagamento;
		public InstituicaoFinanceira instituicaoFinanceira;
		public CanalEntrada canalEntrada;
		public Leitor leitor;
		public MeioCaptura meioCaptura;
		public NumeroLogico numeroLogico;
		public Nsu nsu;
		public CartaoBin cartaoBin;
		public CartaoHolder cartaoHolder;
		public CodigoAutorizacao codigoAutorizacao;
		public CodigoCv codigoCv;

		public class TipoRegistro {
			public Integer inicio;
			public Integer fim;
		}

		public class Estabelecimento {
			public Integer inicio;
			public Integer fim;
		}

		public class DataInicialTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class DataEvento {
			public Integer inicio;
			public Integer fim;
		}

		public class HoraEvento{
			public Integer inicio;
			public Integer fim;
		}

		public class TipoEvento {
			public Integer inicio;
			public Integer fim;
		}

		public class TipoTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class NumeroSerieLeitor {
			public Integer inicio;
			public Integer fim;
		}

		public class CodigoTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class CodigoPedido {
			public Integer inicio;
			public Integer fim;
		}

		public class ValorTotalTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class ValorParcelaOuLiquidoTotal {
			public Integer inicio;
			public Integer fim;
		}

		public class Pagamento {
			public Integer inicio;
			public Integer fim;
		}

		public class Plano {
			public Integer inicio;
			public Integer fim;
		}

		public class Parcela {
			public Integer inicio;
			public Integer fim;
		}

		public class QtdParcelas {
			public Integer inicio;
			public Integer fim;
		}

		public class DataPrevistoPagamento {
			public Integer inicio;
			public Integer fim;
		}

		public class TaxaParcelamentoComprador {
			public Integer inicio;
			public Integer fim;
		}

		public class TarifaBoletoComprador {
			public Integer inicio;
			public Integer fim;
		}

		public class ValorOriginalTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class TaxaParcelamentoVendedor {
			public Integer inicio;
			public Integer fim;
		}

		public class TaxaIntermediacao {
			public Integer inicio;
			public Integer fim;
		}

		public class TarifaItermediacao {
			public Integer inicio;
			public Integer fim;
		}

		public class TarifaBoletoVendedor {
			public Integer inicio;
			public Integer fim;
		}

		public class RepasseAplicacao {
			public Integer inicio;
			public Integer fim;
		}

		public class ValorLiquidoTransacao {
			public Integer inicio;
			public Integer fim;
		}

		public class StatusPagamento {
			public Integer inicio;
			public Integer fim;
		}

		public class Filler {
			public Integer inicio;
			public Integer fim;
		}

		public class MeioPagamento {
			public Integer inicio;
			public Integer fim;
		}

		public class InstituicaoFinanceira {
			public Integer inicio;
			public Integer fim;
		}

		public class CanalEntrada {
			public Integer inicio;
			public Integer fim;
		}

		public class Leitor {
			public Integer inicio;
			public Integer fim;
		}

		public class MeioCaptura {
			public Integer inicio;
			public Integer fim;
		}

		public class NumeroLogico {
			public Integer inicio;
			public Integer fim;
		}

		public class Nsu {
			public Integer inicio;
			public Integer fim;
		}

		public class CartaoBin {
			public Integer inicio;
			public Integer fim;
		}

		public class CartaoHolder {
			public Integer inicio;
			public Integer fim;
		}

		public class CodigoAutorizacao {
			public Integer inicio;
			public Integer fim;
		}

		public class CodigoCv {
			public Integer inicio;
			public Integer fim;
		}

	}

	public class Trailler {

		public TipoRegistro tipoRegistro;
		public TotalRegistro totalRegistro;

		public class TipoRegistro {
			public Integer inicio;
			public Integer fim;
		}

		public class TotalRegistro {
			public Integer inicio;
			public Integer fim;
		}

	}
}
