package br.app.equals.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class BaseJob {

	protected static final Logger logger = LoggerFactory.getLogger(BaseJob.class);
	private static final ReentrantLock processamentoLock = new ReentrantLock();
	private static final String INICIANDO_JOB = "Iniciando job.";


	public static void lock() throws Exception {

		if (!processamentoLock.tryLock(500, TimeUnit.MILLISECONDS)) {
			throw new Exception("Já está em execução");
		}

		logger.info(INICIANDO_JOB);

	}

	public static void unlock() {
		processamentoLock.unlock();
	}


}
