package br.app.equals.job;

import br.app.equals.enums.AdquirenteEnum;
import br.app.equals.strategy.AdquirenteStrategy;
import br.app.equals.util.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Component
public class ProcessarArquivo extends BaseJob {

    @Autowired
    private Environment env;

    private static final String PREFIXO_INICIO_JOB = "\n" +
            "===============================\n" +
            "Iniciando processamento de arquivos.\n" +
            "===============================\n";

    private static final String PREFIXO_FINAL_JOB = "\n" +
            "===============================\n" +
            "Finalizando processamento de arquivos.\n" +
            "===============================\n";

    /**
     * Job que processa os arquivos
     * @throws Exception
     */
    @Scheduled(fixedDelayString = "${job.processaArquivo.intervalo}")
    public void processarArquivos() throws Exception {

        logger.info(PREFIXO_INICIO_JOB);

        File dir = new File(Config.CAMINHO_ARQUIVOS_EM_PROCESSAMENTO);

        File[] files = null;

        if(dir.exists()) {

            files = dir.listFiles();

            if (files.length > 0) {
                Arrays.stream(files)
                    .forEach(file ->{
                        AdquirenteStrategy adquirenteStrategy = AdquirenteStrategy.getInstance(AdquirenteEnum.STONE);
                        try {
                            if(!file.getName().equals(".gitkeep")) {
                                adquirenteStrategy.processarArquivo(file.getName());
                                file.delete();
                            }
                        } catch (IOException e) {
                            logger.error(e.getMessage());
                        }
                    });
            }

            logger.info(PREFIXO_FINAL_JOB);
        }



    }


}
