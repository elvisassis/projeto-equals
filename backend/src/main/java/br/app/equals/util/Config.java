package br.app.equals.util;

public class Config {

	public static final String CAMINHO_ARQUIVOS_EM_PROCESSAMENTO = "arquivo/em_processamento/";
	public static final String CAMINHO_ARQUIVOS_PROCESSADOS = "arquivo/processados";
	public static final String CAMINHO_CONFIGURACAO_ADQUIRENTE = "configuracao_adquirente/";

	public static final String DEFAULT_TIME_ZONE_CODE = "America/Sao_Paulo";


}
