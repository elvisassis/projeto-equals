package br.app.equals.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {

    public static String formataData(String data) {

       StringBuilder stringBuilder = new StringBuilder(data);

       stringBuilder.insert(4, '-');
       stringBuilder.insert(7, '-');
       return stringBuilder.toString();
    }

    public static String formataHora(String hora) {

        StringBuilder stringBuilder = new StringBuilder(hora);

        stringBuilder.insert(2, ':');
        stringBuilder.insert(5, ':');
        return stringBuilder.toString();
    }

}
