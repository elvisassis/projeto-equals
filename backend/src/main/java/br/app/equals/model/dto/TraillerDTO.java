package br.app.equals.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TraillerDTO {

    private Integer totalRegistros;
    private Integer tipoRegistro;

}
