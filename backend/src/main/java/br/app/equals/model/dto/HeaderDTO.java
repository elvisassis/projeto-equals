package br.app.equals.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HeaderDTO {

    private Integer tipoRegistro;
    private Integer estabelecimento;
    private LocalDate dataProcessamento;
    private LocalDate periodoInicial;
    private LocalDate periodoFinal;
    private Integer sequencia;
    private String empresaAdquirente;
    private Integer tipoExtrato;
    private String filler;
    private String versaoLayout;
    private String versaoRelease;

}
