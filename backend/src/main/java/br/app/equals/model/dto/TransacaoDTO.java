package br.app.equals.model.dto;

import br.app.equals.model.entities.Transacao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransacaoDTO {


    private String dataInicialTransacao;
    private String dataEvento;
    private String dataEventoFinal;
    private String horaEvento;
    private Double valorTransacao;
    private Double valorParcelaOuLiquidoTotal;
    private String parcela;
    private Integer qtdParcelas;
    private String dataPrevistaPagamento;
    private Double valorOriginalTransacao;
    private Double taxaParcelamentoVendedor;
    private Double taxaIntermediacao;
    private Double tarifaIntermediacao;
    private Double tarifaBoletoVendedor;
    private Double repasseAplicacao;
    private Double valorLiquidoTransacao;
    private String instituicaoFinanceira;

    public TransacaoDTO (Transacao transacao) {
        this.dataInicialTransacao = transacao.getDataInicialTransacao().toString();
        this.dataEvento = transacao.getDataEvento().toString();
        this.horaEvento = transacao.getHoraEvento().toString();
        this.valorTransacao = transacao.getValorTransacao();
        this.valorParcelaOuLiquidoTotal = transacao.getValorParcelaOuLiquidoTotal();
        this.parcela = StringUtils.trim(transacao.getParcela());
        this.qtdParcelas = transacao.getQtdParcelas();
        this.dataPrevistaPagamento = transacao.getDataPrevistaPagamento().toString();
        this.valorOriginalTransacao = transacao.getValorOriginalTransacao();
        this.taxaParcelamentoVendedor = transacao.getTaxaParcelamentoVendedor();
        this.taxaIntermediacao = transacao.getTaxaIntermediacao();
        this.tarifaIntermediacao = transacao.getTarifaIntermediacao();
        this.tarifaBoletoVendedor = transacao.getTarifaBoletoVendedor();
        this.repasseAplicacao = transacao.getRepasseAplicacao();
        this.valorLiquidoTransacao = transacao.getValorLiquidoTransacao();
        this.instituicaoFinanceira = StringUtils.trim(transacao.getInstituicaoFinanceira());
    }

}
