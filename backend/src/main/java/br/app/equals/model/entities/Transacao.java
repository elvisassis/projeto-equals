package br.app.equals.model.entities;


import br.app.equals.configuracao.ConfiguracaStone;
import br.app.equals.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(schema = "public", name = "transacao")
public class Transacao {

	private static final long serialVersionUID = 1L;

	private static final String SEQUENCE = "public" + ".transacao_id_seq";

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	private Long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_header")
	private Header header;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_trailler")
	private Trailler trailler;

	@JsonIgnore
	@Column(name = "tipo_registro")
	private Integer tipoDeRegistro;

	@Column(name = "estabelecimento")
	private Integer estabelecimento;

	@Column(name = "data_inicio_transacao")
	private LocalDate dataInicialTransacao;

	@Column(name = "data_evento")
	private LocalDate dataEvento;

	@Column(name = "hora_evento")
	private LocalTime horaEvento;

	@Column(name = "tipo_evento")
	private Integer tipoEvento;

	@Column(name = "tipo_transacao")
	private  Integer tipoTransacao;

	@Column(name = "numero_serie_leitor")
	private String numeroSerieLeitor;

	@Column(name = "codigo_transacao")
	private String codigoTransacao;

	@Column(name = "codigo_pedido")
	private String codigoPedido;

	@Column(name = "valor_total_transacao")
	private Double valorTransacao;

	@Column(name = "valor_parcela_liquida_ou_total")
	private Double valorParcelaOuLiquidoTotal;

	@Column(name = "pagamento")
	private String pagamento;

	@Column(name = "plano")
	private  String plano;

	@Column(name = "parcela")
	private String parcela;

	@Column(name = "qtd_parcelas_transacao")
	private Integer qtdParcelas;

	@Column(name = "data_prevista_pagamento")
	private LocalDate dataPrevistaPagamento;

	@Column(name = "taxa_parcelamento_comprador")
	private Double taxaParcelamentoComprador;

	@Column(name = "tarifa_boleto_comprador")
	private Double tarifaBoletoComprador;

	@Column(name = "valor_original_transacao")
	private Double valorOriginalTransacao;

	@Column(name = "taxa_parcelamento_vendedor")
	private Double taxaParcelamentoVendedor;

	@Column(name = "taxa_intermediacao")
	private Double taxaIntermediacao;

	@Column(name = "tarifa_intermediacao")
	private Double tarifaIntermediacao;

	@Column(name = "tarifa_boleto_vendedor")
	private Double tarifaBoletoVendedor;

	@Column(name = "repasse_aplicacao")
	private Double repasseAplicacao;

	@Column(name = "valor_liquido_transacao")
	private Double valorLiquidoTransacao;

	@Column(name = "status_pagamento")
	private Integer statusPagamento;

	@Column(name = "filler")
	private String filler;

	@Column(name = "meio_pagamento")
	private Integer meioPagamento;

	@Column(name = "instituicao_financeira")
	private String instituicaoFinanceira;

	@Column(name = "canal_entrada")
	private String canalEntrada;

	@Column(name = "leitor")
	private Integer leitor;

	@Column(name = "meio_captura")
	private Integer meioCaptura;

	@Column(name = "numero_logico")
	private String numeroLogico;

	@Column(name = "nsu")
	private String nsu;

	@Column(name = "cartao_bin")
	private String cartaoBin;

	@Column(name = "cartao_holder")
	private String cartaoHolder;

	@Column(name = "codigo_autorizacao")
	private String codigoAutorizacao;

	@Column(name = "codigo_cv")
	private String codigoCv;

	public Transacao (String transacao, Header header, Trailler trailler, ConfiguracaStone config) {

		ConfiguracaStone.Transacao configTransacao = config.getTransacao();

		this.header = header;
		this.trailler = trailler;
		this.tipoDeRegistro = Integer.parseInt(transacao.substring(configTransacao.tipoRegistro.inicio, configTransacao.tipoRegistro.fim));
		this.estabelecimento = Integer.parseInt(transacao.substring(configTransacao.estabelecimento.inicio, configTransacao.estabelecimento.fim));
		this.dataInicialTransacao = LocalDate.parse(DateUtil.formataData(transacao.substring(configTransacao.dataInicialTransacao.inicio, configTransacao.dataInicialTransacao.fim)));
		this.dataEvento = LocalDate.parse(DateUtil.formataData(transacao.substring(configTransacao.dataEvento.inicio, configTransacao.dataEvento.fim)));
		this.horaEvento = LocalTime.parse(DateUtil.formataHora(transacao.substring(configTransacao.horaEvento.inicio, configTransacao.horaEvento.fim)));
		this.tipoEvento = Integer.parseInt(transacao.substring(configTransacao.tipoEvento.inicio, configTransacao.tipoEvento.fim));
		this.tipoTransacao = Integer.parseInt(transacao.substring(configTransacao.tipoTransacao.inicio, configTransacao.tipoTransacao.fim));
		this.numeroSerieLeitor = transacao.substring(configTransacao.numeroSerieLeitor.inicio, configTransacao.numeroSerieLeitor.fim);
		this.codigoTransacao = transacao.substring(configTransacao.codigoTransacao.inicio, configTransacao.codigoTransacao.fim);
		this.codigoPedido = transacao.substring(configTransacao.codigoPedido.inicio, configTransacao.codigoPedido.fim);
		this.valorTransacao = Double.parseDouble(transacao.substring(configTransacao.valorTotalTransacao.inicio, configTransacao.valorTotalTransacao.fim));
		this.valorParcelaOuLiquidoTotal = Double.parseDouble(transacao.substring(configTransacao.valorParcelaOuLiquidoTotal.inicio, configTransacao.valorParcelaOuLiquidoTotal.fim));
		this.pagamento = transacao.substring(configTransacao.pagamento.inicio, configTransacao.pagamento.fim);
		this.plano = transacao.substring(configTransacao.plano.inicio, configTransacao.plano.fim);
		this.parcela = transacao.substring(configTransacao.parcela.inicio, configTransacao.parcela.fim);
		this.qtdParcelas = Integer.parseInt(transacao.substring(configTransacao.qtdParcelas.inicio, configTransacao.qtdParcelas.fim));
		this.dataPrevistaPagamento = LocalDate.parse(DateUtil.formataData(transacao.substring(configTransacao.dataPrevistoPagamento.inicio, configTransacao.dataPrevistoPagamento.fim)));
		this.taxaParcelamentoComprador = Double.parseDouble(transacao.substring(configTransacao.taxaParcelamentoComprador.inicio, configTransacao.taxaParcelamentoComprador.fim));
		this.tarifaBoletoComprador = Double.parseDouble(transacao.substring(configTransacao.tarifaBoletoComprador.inicio, configTransacao.tarifaBoletoComprador.fim));
		this.valorOriginalTransacao = Double.parseDouble(transacao.substring(configTransacao.valorOriginalTransacao.inicio, configTransacao.valorOriginalTransacao.fim));
		this.taxaParcelamentoVendedor = Double.parseDouble(transacao.substring(configTransacao.taxaParcelamentoVendedor.inicio, configTransacao.taxaParcelamentoVendedor.fim));
		this.taxaIntermediacao = Double.parseDouble(transacao.substring(configTransacao.taxaIntermediacao.inicio, configTransacao.taxaIntermediacao.fim));
		this.tarifaIntermediacao = Double.parseDouble(transacao.substring(configTransacao.tarifaItermediacao.inicio, configTransacao.tarifaItermediacao.fim));
		this.tarifaBoletoVendedor = Double.parseDouble(transacao.substring(configTransacao.tarifaBoletoVendedor.inicio, configTransacao.tarifaBoletoVendedor.fim));
		this.repasseAplicacao = Double.parseDouble(transacao.substring(configTransacao.repasseAplicacao.inicio, configTransacao.repasseAplicacao.fim));
		this.valorLiquidoTransacao = Double.parseDouble(transacao.substring(configTransacao.valorLiquidoTransacao.inicio, configTransacao.valorLiquidoTransacao.fim));
		this.statusPagamento = Integer.parseInt(transacao.substring(configTransacao.statusPagamento.inicio, configTransacao.statusPagamento.fim));
		this.filler = transacao.substring(configTransacao.filler.inicio, configTransacao.filler.fim);
		this.meioPagamento = Integer.parseInt(transacao.substring(configTransacao.meioPagamento.inicio, configTransacao.meioPagamento.fim));
		this.instituicaoFinanceira = transacao.substring(configTransacao.instituicaoFinanceira.inicio, configTransacao.instituicaoFinanceira.fim);
		this.canalEntrada = transacao.substring(configTransacao.canalEntrada.inicio, configTransacao.canalEntrada.fim);
		this.leitor = Integer.parseInt(transacao.substring(configTransacao.leitor.inicio, configTransacao.leitor.fim));
		this.meioCaptura = Integer.parseInt(transacao.substring(configTransacao.meioCaptura.inicio, configTransacao.meioCaptura.fim));
		this.numeroLogico = transacao.substring(configTransacao.numeroLogico.inicio, configTransacao.numeroLogico.fim);
		this.nsu = transacao.substring(configTransacao.nsu.inicio, configTransacao.nsu.fim);
		this.cartaoBin = transacao.substring(configTransacao.cartaoBin.inicio, configTransacao.cartaoBin.fim);
		this.cartaoHolder = transacao.substring(configTransacao.cartaoHolder.inicio, configTransacao.cartaoHolder.fim);
		this.codigoAutorizacao = transacao.substring(configTransacao.codigoAutorizacao.inicio, configTransacao.codigoAutorizacao.fim);
		this.codigoCv = transacao.substring(configTransacao.codigoCv.inicio, configTransacao.codigoCv.fim);
	}


}
