package br.app.equals.model.entities;


import br.app.equals.configuracao.ConfiguracaStone;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "public", name = "trailler")
public class Trailler {

	private static final long serialVersionUID = 1L;

	private static final String SEQUENCE = "public" + ".trailler_id_seq";

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "tipo_registro")
	private Integer tipoRegistro;

	@Column(name = "total_registros")
	private Integer totalRegistros;

	@OneToMany(mappedBy = "trailler", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<Transacao> transacoes;

	public Trailler (String trailler, ConfiguracaStone.Trailler config) {

		this.totalRegistros = Integer.parseInt(trailler.substring(config.totalRegistro.inicio, config.totalRegistro.fim));
		this.tipoRegistro = Integer.parseInt(trailler.substring(config.tipoRegistro.inicio, config.tipoRegistro.fim));
	}
}
