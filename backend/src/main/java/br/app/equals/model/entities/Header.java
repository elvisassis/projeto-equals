package br.app.equals.model.entities;

import br.app.equals.configuracao.ConfiguracaStone;
import br.app.equals.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "public", name = "header")
public class Header {

	private static final long serialVersionUID = 1L;

	private static final String SEQUENCE = "public" + ".header_id_seq";

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "tipo_registro")
	private Integer tipoRegistro;

	@Column(name = "estabelecimento")
	private Integer estabelecimento;

	@Column(name = "data_processamento")
	private LocalDate dataProcessamento;

	@Column(name = "periodo_inicial")
	private LocalDate periodoInicial;

	@Column(name = "periodo_final")
	private LocalDate periodoFinal;

	@Column(name = "sequencia")
	private Integer sequencia;

	@Column(name = "empresa_adquirente")
	private String empresaAdquirente;

	@Column(name = "tipo_extrato")
	private Integer tipoExtrato;

	@Column(name = "filler")
	private String filler;

	@Column(name = "versao_layout")
	private String versaoLayout;

	@Column(name = "versao_release")
	private String versaoRelease;

	@OneToMany(mappedBy = "header", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<Transacao> transacoes;

	public Header (String header, ConfiguracaStone.Header config) {

		this.tipoRegistro = Integer.parseInt(header.substring(config.tipoRegistro.inicio, config.tipoRegistro.fim));
		this.estabelecimento = Integer.parseInt(header.substring(config.estabelecimento.inicio, config.estabelecimento.fim));
		this.dataProcessamento = LocalDate.parse(DateUtil.formataData(header.substring(config.dataProcessamento.inicio, config.dataProcessamento.fim)));
		this.periodoInicial = LocalDate.parse(DateUtil.formataData(header.substring(config.periodoInicial.inicio, config.periodoInicial.fim)));
		this.periodoFinal = LocalDate.parse(DateUtil.formataData(header.substring(config.periodoFinal.inicio, config.periodoFinal.fim)));
		this.sequencia = Integer.parseInt(header.substring(config.sequencia.inicio, config.sequencia.fim));
		this.empresaAdquirente = header.substring(config.empresaAdiquirente.inicio, config.empresaAdiquirente.fim);
		this.tipoExtrato = Integer.parseInt(header.substring(config.tipoExtrato.inicio, config.tipoExtrato.fim));
		this.filler = header.substring(config.filler.inicio, config.filler.fim);
		this.versaoLayout = header.substring(config.versaoLayout.inicio, config.versaoLayout.fim);
		this.versaoRelease = header.substring(config.versaoRelease.inicio, config.versaoRelease.fim);
	}
}
