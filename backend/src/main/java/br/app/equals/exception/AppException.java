package br.app.equals.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AppException extends RuntimeException {

	private static final long serialVersionUID = 4657491283614755649L;

	public AppException(String mensagem, Object... args) {
		
		super("Contate o adminstrador do sistemas");
	}

	public AppException(String mensagem, Throwable causa) {
		
		super(mensagem, causa);
	}

}
