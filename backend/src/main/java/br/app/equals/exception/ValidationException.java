package br.app.equals.exception;

import javax.validation.ConstraintViolation;
import java.util.Collections;
import java.util.Set;

public class ValidationException extends AppException {

	private static final long serialVersionUID = -47332445987710531L;
	
	private Set<ConstraintViolation<?>> errors;
	
	public ValidationException(String mensagem, Object... args) {
		
		super(mensagem, args);
	}

	
	public <T> ValidationException(Set<ConstraintViolation<T>> errors) {
		
		super("Contate o Administrador do sistemas");
		this.errors = Collections.unmodifiableSet(errors);
	}


	public Set<ConstraintViolation<?>> getErrors() {
		return errors;
	}
}
