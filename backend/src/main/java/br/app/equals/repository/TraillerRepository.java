package br.app.equals.repository;

import br.app.equals.model.entities.Trailler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TraillerRepository extends JpaRepository<Trailler, Long> {
}
