package br.app.equals.repository.specification;

import br.app.equals.model.dto.TransacaoDTO;
import br.app.equals.model.entities.Transacao;
import br.app.equals.util.Config;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.ZoneId;

public class TransacaoSpecification {

    public static Specification<Transacao> filtroTransacaoConsulta(TransacaoDTO transacao) {
        return Specification.where(distinct())
                .and(comDataVendaInicial(transacao.getDataEvento()))
                .and(comDataVendaFinal(transacao.getDataEventoFinal()));
    }


    private static Specification<Transacao> distinct() {
        return (root, query, criteriaBuilder) -> {
            query.distinct(true);
            return null;
        };
    }

    private static Specification<Transacao> comDataVendaInicial(String dataVendaInicial) {

        if(dataVendaInicial == null) {
            return null;
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("dataEvento"), LocalDate.parse(dataVendaInicial)));
    }

    private static Specification<Transacao> comDataVendaFinal(String dataVendaFinal) {

        if(dataVendaFinal == null) {
            return null;
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("dataEvento"), LocalDate.parse(dataVendaFinal)));
    }



}
