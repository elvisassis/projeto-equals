package br.app.equals.strategy;

import br.app.equals.util.ApplicationContextUtils;

import java.io.IOException;

public class Adquirente implements AdquirenteStrategy {


	@Override
	public void processarArquivo(String fileName) throws IOException {
		throw new RuntimeException("Este método deve ser sobrescrito!");
	}

	public <T> T getBean(Class<T> beanClass) {

		return ApplicationContextUtils.getBean(beanClass);
	}
}
