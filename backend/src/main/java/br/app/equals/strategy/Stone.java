package br.app.equals.strategy;

import br.app.equals.configuracao.ConfiguracaStone;
import br.app.equals.model.entities.Header;
import br.app.equals.model.entities.Trailler;
import br.app.equals.model.entities.Transacao;
import br.app.equals.service.HeaderService;
import br.app.equals.service.TraillerService;
import br.app.equals.service.TransacaoService;
import br.app.equals.util.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Stone extends Adquirente {

	private final Gson GSON = new GsonBuilder().create();

	private TransacaoService transacaoService;

	private HeaderService headerService;

	private TraillerService traillerService;

	public Supplier<Stream<String>> getFile() throws IOException {

		Path path = Paths.get(Config.CAMINHO_ARQUIVOS_EM_PROCESSAMENTO + "processoSeletivoEquals.txt");
		Supplier<Stream<String>> linhas = () -> {
			try {
				return Files.lines(path);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		};
		return linhas;
	}

	@Override
	public void processarArquivo(String fileName) throws IOException {

		Supplier<Stream<String>> file = getFile();

		String lineHeader = file.get().findFirst().orElse(null);

		String lineTrailler = file.get().reduce((first, second) -> second).orElse(null);

		List<String> transacoes = file.get().
				filter(transacao -> !transacao.equals(lineHeader) && !transacao.equals(lineTrailler))
				.collect(Collectors.toList());

		ConfiguracaStone config = GSON.fromJson(new FileReader(Config.CAMINHO_CONFIGURACAO_ADQUIRENTE + "adquirente.json"), ConfiguracaStone.class);

		transacaoService = getBean(TransacaoService.class);
		headerService = getBean(HeaderService.class);
		traillerService = getBean(TraillerService.class);

		Header header = new Header(lineHeader, config.getHeader());
		headerService.save(header);

		Trailler trailler = new Trailler(lineTrailler, config.getTrailler());
		traillerService.save(trailler);

		transacaoService = getBean(TransacaoService.class);

		transacoes.forEach(transacao -> {
			transacaoService.save(new Transacao(transacao, header, trailler, config));
		});


		//Integer tipoRegisto = Integer.parseInt(header.substring(config.header.tipoRegistro.inicio, config.header.tipoRegistro.fim));
		//Integer estabelecimento = Integer.parseInt(header.substring(config.header.estabelecimento.inicio, config.header.estabelecimento.fim));
		//Integer dataProcessamento = Integer.parseInt(header.substring(config.header.dataProcessamento.inicio, config.header.dataProcessamento.fim));

	}
}
