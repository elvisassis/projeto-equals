package br.app.equals.strategy;

import br.app.equals.enums.AdquirenteEnum;

import java.io.IOException;

public interface AdquirenteStrategy {

	void processarArquivo(String fileName) throws IOException;


	static AdquirenteStrategy getInstance(AdquirenteEnum adquirente) {

		if(adquirente.equals(AdquirenteEnum.STONE)){
			return new Stone();
		}

		return null;
	}
}
