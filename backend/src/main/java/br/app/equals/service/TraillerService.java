package br.app.equals.service;

import br.app.equals.model.entities.Trailler;
import br.app.equals.repository.HeaderRepository;
import br.app.equals.repository.TraillerRepository;
import org.springframework.stereotype.Service;

@Service
public class TraillerService {

    private final TraillerRepository traillerRepository;

    public TraillerService(TraillerRepository traillerRepository) {
        this.traillerRepository = traillerRepository;
    }

    public void save (Trailler trailler) {
        traillerRepository.save(trailler);
    }
}
