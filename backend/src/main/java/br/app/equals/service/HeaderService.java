package br.app.equals.service;

import br.app.equals.model.entities.Header;
import br.app.equals.repository.HeaderRepository;
import org.springframework.stereotype.Service;

@Service
public class HeaderService {

    private final HeaderRepository headerRepository;

    public HeaderService(HeaderRepository headerRepository) {
        this.headerRepository = headerRepository;
    }

    public void save (Header header) {
        headerRepository.save(header);
    }
}
