package br.app.equals.service;

import br.app.equals.model.dto.TransacaoDTO;
import br.app.equals.model.entities.Transacao;
import br.app.equals.repository.TransacaoRepository;
import br.app.equals.repository.specification.TransacaoSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TransacaoService {

    private final TransacaoRepository transacaoRepository;

    public TransacaoService(TransacaoRepository transacaoRepository) {
        this.transacaoRepository = transacaoRepository;
    }

    public void save (Transacao transacao) {
        transacaoRepository.save(transacao);
    }

    /**
     * Método que retorna as transações de acordo com os filtros
     * @param transacaoDTO
     * @return
     */
    public List<TransacaoDTO> findByFiltros(TransacaoDTO transacaoDTO) {

        Specification<Transacao> specs = TransacaoSpecification.filtroTransacaoConsulta(transacaoDTO);

        Supplier<Stream<Transacao>> result = () -> transacaoRepository.findAll(specs).stream();

        List<Transacao> transacoes = result.get().collect(Collectors.toList());

        List<TransacaoDTO> transacoesDTO = new ArrayList<>();

        transacoes.forEach(transacao -> transacoesDTO.add(new TransacaoDTO(transacao)));

        return transacoesDTO;

    }
}
