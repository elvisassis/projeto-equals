import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Relatorio from './views/Relatorio.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/relatorio',
			name: 'relatorio',
			component: Relatorio
		}
	]
});
