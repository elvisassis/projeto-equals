
export default {

	system: {
		defaultError: 'Ocorreu um erro inesperado, contate o adminstrador do sistema'
	},

	validation: {
		name: 'pt_BR',
		messages: {
			required: 'Campo obrigatório.',
			start_alpha_only: 'Campo deve iniciar com uma letra.',
			min_value: (e, n) => { return 'O valor deste campo deve ser maior ou igual a ' + n[0] + '.'; },
			decimal: 'O campo deve ser um número decimal válido',
			date_format: 'Data inválida ou não está no padrão dia/mês/ano',
			before: 'Data inicial deve ser anterior a data final',
			after: 'Data final deve ser posterior a data inicial'
		}
	}
	
};
