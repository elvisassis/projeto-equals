
import _ from 'lodash';
import VeeValidate from 'vee-validate';
import Messages from './messages';


// Configurando Locale
VeeValidate.Validator.addLocale(Messages.validation);

// Método para adicionar validação customizada para componente
VeeValidate.Validator.prototype.addComponentRule = function (vm, customRule) {

	if (!vm.$el) {
		return;
	}

	var fieldId = vm.$el.getAttribute('data-vv-id');
	
	if (!fieldId) {

		console.error('Ainda não implementado o tratamento para este caso.');
		return;

	} else {
		
		let field = this.fields.find({ id: fieldId });
		let ruleName = 'custom_rule_' + ((vm.$options && vm.$options.name) || '') + field.id;
		this.extend(ruleName, customRule);
		field.rules = field.rules || {};
		field.rules[ruleName] = [];
		field.update({ rules: field.rules });
	}
};

VeeValidate.Validator.prototype.cleanErrors = _.debounce(function (names) {

	if (names) {
		names = (names instanceof Array) ? names : [ names ];
	}

	for (let field of this.fields.items) {
		
		if (!names || names.includes(field.name)) {

			this.errors.removeById(field.id);
			field.flags.invalid = false;
			field.flags.valid = true;
			field.updateClasses();
		}
	}
});
