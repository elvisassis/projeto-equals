import http from './http';

export default {

	findAll (callback) {
		return http.get({
			url: '/relatorio-vendas',
			success: callback
		});
	},

	findByFiltros(data, callback) {
		return http.post({
			url: '/relatorio-vendas',
			data: data,
			success: callback
		});
	},

	save (data, callback) {
		return http.post({
			url: '/exemplos',
			data: data,
			success: callback
		});
	},

	edit (data, callback) {
		return http.put({
			url: '/exemplos/' + data.id,
			data: data,
			success: callback
		});
	},

	delete (data, callback) {
		return http.delete({
			url: '/exemplos/' + data.id,
			success: callback
		});
	}
};
