import loading from '@/utils/loading';
import axios from 'axios';
import alert from '@/utils/alert';
import Messages from '@/utils/messages';

const BASE_URL = process.env.APPLICATION_PATH ? process.env.APPLICATION_PATH : "";

axios.defaults.timeout = 3600000;

const HttpStatus = {
	UNAUTHORIZED: 401
};

export default class Http {

	static baseURL () {
		return BASE_URL;
	}

	static _getOptionValue (options, prop, defaultValue) {

		if (!options || !options.hasOwnProperty(prop)) {
			return defaultValue;
		} else {
			return options[prop];
		}
	}

	static _handleRequestPromisse (promisse, options) {

		promisse.then(response => {

			if (options.lockScreen) {
				loading.hide();
			}

			if (options.success) {
				options.success(response.data);
			}

		}).catch(error => {
			
			if (error.request.status === HttpStatus.UNAUTHORIZED) {

				window.location.href = BASE_URL + '/login';
			}
			
			if (options.error) {
				
				options.error(error.response.data);

			} else {

				alert.error(Messages.system.defaultError);
			}

			if (options.lockScreen) {
				loading.hide();
			}
		});
	}

	static _setDefaultOptions (options) {

		options = options || {};
		options.fullUrl = BASE_URL + options.url;
		options.lockScreen = Http._getOptionValue(options, 'lockScreen', true);
		options.lockMessage = options.lockMessage || 'Carregando';
		
		return options;
	}

	static get (options) {

		options = Http._setDefaultOptions(options);

		if (options.lockScreen) {
			loading.show(options.lockMessage);
		}

		var promisse = options.data ? axios.get(options.fullUrl, { params: options.data }) : axios.get(options.fullUrl);

		Http._handleRequestPromisse(promisse, options);

		return promisse;
	}
	
	static post (options) {
		
		options = Http._setDefaultOptions(options);

		if (options.lockScreen) {
			loading.show(options.lockMessage);
		}

		var promisse = axios.post(options.fullUrl, options.data);
		
		Http._handleRequestPromisse(promisse, options);

		return promisse;
	}

	static delete (options) {
		
		options = Http._setDefaultOptions(options);

		if (options.lockScreen) {
			loading.show(options.lockMessage);
		}

		var promisse = options.data ? axios.delete(options.fullUrl, { params: options.data }) : axios.delete(options.fullUrl);

		Http._handleRequestPromisse(promisse, options);

		return promisse;
	}

	static put (options) {
		
		options = Http._setDefaultOptions(options);

		if (options.lockScreen) {
			loading.show(options.lockMessage);
		}

		var promisse = axios.put(options.fullUrl, options.data);
		
		Http._handleRequestPromisse(promisse, options);

		return promisse;
	}
}
