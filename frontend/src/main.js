import 'jquery';
import 'bootstrap';
import 'blockui';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VeeValidate from 'vee-validate';
import './utils/validations';
import BootstrapVue from 'bootstrap-vue'
import InputErrors from '@/components/InputErrors';

import '@fortawesome/fontawesome-free/js/all.js';
import '@fortawesome/fontawesome-free/css/all.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css';

import '@/styles/principal.scss';

Vue.config.productionTip = false;

Vue.use(VeeValidate, {classes: true, locale: 'pt_BR'});
Vue.use(BootstrapVue);

Vue.component('input-errors', InputErrors);

new Vue({
	router,
	render: h => h(App)
}).$mount('#app');
