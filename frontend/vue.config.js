webpack = require('webpack');

module.exports = {

	// where to output built files
	outputDir: '../backend/src/main/resources/static',

	// configure webpack-dev-server behavior
	devServer: {
		open: process.platform === 'darwin',
		host: '0.0.0.0',
		port: 8081,
		https: false,
		hotOnly: false,
		proxy: 'http://localhost:8050'
	},
	
	configureWebpack: {
		plugins: [
			new webpack.ProvidePlugin({
				'$': 'jquery',
				jQuery : 'jquery',
				jquery: 'jquery',
				'_': 'lodash'
			}),
		]
	}
};